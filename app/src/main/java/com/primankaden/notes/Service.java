package com.primankaden.notes;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.primankaden.notes.db.DataBase;

import java.util.Date;

public class Service extends IntentService {
    public Service() {
        super(IntentService.class.getName() + new Date().getTime());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null || intent.getAction() == null) {
            return;
        }
        switch (intent.getAction()) {
            case ADD_ACTION:
                Note n = intent.getParcelableExtra(EXTRA_TAG);
                DataBase.addNote(n);
                break;
            case DELETE_ACTION:
                DataBase.deleteNote(intent.getIntExtra(EXTRA_TAG, -1));
                break;
            case UPDATE_ACTION:
                Note note = intent.getParcelableExtra(EXTRA_TAG);
                DataBase.updateNote(note);
                break;
            default:
        }
    }

    private static final String ADD_ACTION = "addAction";
    private static final String UPDATE_ACTION = "updateAction";
    private static final String DELETE_ACTION = "deleteAction";
    private static final String EXTRA_TAG = "extra";

    public static void addNote(Context from, Note note) {
        Intent i = new Intent(from, Service.class);
        i.setAction(ADD_ACTION);
        i.putExtra(EXTRA_TAG, note);
        from.startService(i);
    }


    public static void updateNote(Context from, Note note) {
        Intent i = new Intent(from, Service.class);
        i.setAction(UPDATE_ACTION);
        i.putExtra(EXTRA_TAG, note);
        from.startService(i);
    }

    public static void deleteNote(Context from, int noteId) {
        Intent i = new Intent(from, Service.class);
        i.setAction(DELETE_ACTION);
        i.putExtra(EXTRA_TAG, noteId);
        from.startService(i);
    }

}
