package com.primankaden.notes;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

public class Note implements Parcelable {
    public final int id;
    public final String title;
    public final String description;
    public final long date;
    public final double latitude;
    public final double longitude;
    public final Importance importance;

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        if (title != null && !title.isEmpty()) {
            b.append(title).append("\n");
        }
        if (description != null && !description.isEmpty()) {
            b.append(description).append("\n");
        }
        b.append("http://maps.google.com/maps?saddr=").append(latitude).append(",").append(longitude);
        return b.toString();
    }

    public enum Importance {
        MAJOR(R.color.major, R.string.major),
        NORMAL(R.color.normal, R.string.normal),
        MINOR(R.color.minor, R.string.minor),
        NONE(R.color.none, R.string.none);

        private int color, title;

        public int getTitleRes() {
            return title;
        }

        public static String[] getTitlesArray() {
            String[] result = new String[values().length];
            for (Importance i : values()) {
                result[i.ordinal()] = App.getContext().getString(i.title);
            }
            return result;
        }

        public int getColorRes() {
            return color;
        }

        Importance(int color, int title) {
            this.color = color;
            this.title = title;
        }
    }

    public LatLng getLatLng() {
        return new LatLng(latitude, longitude);
    }

    public Note(int id, String title, String description, long date, double latitude, double longitude, Importance importance) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.latitude = latitude;
        this.longitude = longitude;
        this.importance = importance;
    }

    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel source) {
            return new Note(source);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    private Note(Parcel source) {
        id = source.readInt();
        title = source.readString();
        description = source.readString();
        date = source.readLong();
        latitude = source.readDouble();
        longitude = source.readDouble();
        importance = Importance.values()[source.readInt()];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeLong(date);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(importance.ordinal());
    }

    public void shareMe(Context from) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, toString());
        shareIntent.setType("text/*");
        from.startActivity(Intent.createChooser(shareIntent, from.getString(R.string.share_note)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}
