package com.primankaden.notes;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.ExceptionParser;
import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class App extends Application {
    private static final String AD_MOB_ID = "ca-app-pub-3108224483396158/3126957625";
    private static Context mContext;
    private static Tracker mTracker;
    private static InterstitialAd mInterstitialAd;
    private static int mGoogleApiAvailability = -1;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        mTracker = analytics.newTracker(R.xml.analytic_tracker);
        mTracker.enableAdvertisingIdCollection(true);
        ExceptionReporter uncaughtExceptionHandler = new ExceptionReporter(mTracker,
                Thread.getDefaultUncaughtExceptionHandler(), mContext);
        uncaughtExceptionHandler.setExceptionParser(new AnalyticsExceptionParser());
        Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(AD_MOB_ID);
    }

    public static Context getContext() {
        return mContext;
    }

    public static boolean checkGoogleApiAvailability() {
        if (mGoogleApiAvailability < 0) {
            mGoogleApiAvailability = GoogleApiAvailability.getInstance()
                    .isGooglePlayServicesAvailable(mContext);
        }
        return mGoogleApiAvailability == ConnectionResult.SUCCESS;
    }

    public static InterstitialAd getAd() {
        return mInterstitialAd;
    }

    public static Tracker getAnalyticsTracker() {
        return mTracker;
    }

    public class AnalyticsExceptionParser implements ExceptionParser {
        public String getDescription(String p_thread, Throwable p_throwable) {
            return "Thread: " + p_thread + ", " + p_throwable.getLocalizedMessage() + ", "
                    + p_throwable.toString();
        }
    }
}
