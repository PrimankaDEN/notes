package com.primankaden.notes.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.primankaden.notes.App;
import com.primankaden.notes.Note;

import java.util.ArrayList;
import java.util.List;

import static com.primankaden.notes.db.DBContract.COLUMNS;
import static com.primankaden.notes.db.DBContract.COLUMN_COORDS_LAT;
import static com.primankaden.notes.db.DBContract.COLUMN_COORDS_LONG;
import static com.primankaden.notes.db.DBContract.COLUMN_DATE;
import static com.primankaden.notes.db.DBContract.COLUMN_DESCRIPTION;
import static com.primankaden.notes.db.DBContract.COLUMN_ID;
import static com.primankaden.notes.db.DBContract.COLUMN_IMPORTANCE;
import static com.primankaden.notes.db.DBContract.COLUMN_TITLE;

public class DataBase extends SQLiteOpenHelper {
    public static final String DB_NAME = "noteDbName";
    private static DataBase instance;

    public DataBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public static DataBase getInstance() {
        if (instance == null) {
            instance = new DataBase(App.getContext(), DB_NAME, null, 1);
        }
        return instance;
    }

    public static Cursor getNotes() {
        return App.getContext().getContentResolver().query(NoteContentProvider.URI, COLUMNS,
                null, null, COLUMN_DATE);
    }

    public static List<Note> cursorToNotes(Cursor c) {
        List<Note> list = new ArrayList<>();
        while (c.moveToNext()) {
            Note e = fromCursor(c);
            list.add(e);
        }
        return list;
    }

    public static Cursor getNote(int id) {
        return App.getContext().getContentResolver().query(NoteContentProvider.URI, COLUMNS, COLUMN_ID + " = " + id, null, null);
    }

    public static void addNote(Note note) {
        ContentValues cv = noteToContentValues(note);
        App.getContext().getContentResolver().insert(NoteContentProvider.URI, cv);
    }

    public static void deleteNote(int id) {
        App.getContext().getContentResolver().delete(NoteContentProvider.URI, COLUMN_ID + " = " + id, null);
    }

    public static void updateNote(Note note) {
        ContentValues cv = noteToContentValues(note);
        App.getContext().getContentResolver().update(NoteContentProvider.URI, cv, COLUMN_ID + " = " + note.id, null);
    }

    private static ContentValues noteToContentValues(Note e) {
        ContentValues c = new ContentValues();
        c.put(COLUMN_ID, e.id);
        c.put(COLUMN_TITLE, e.title);
        c.put(COLUMN_DESCRIPTION, e.description);
        c.put(COLUMN_DATE, e.date);
        c.put(COLUMN_COORDS_LAT, e.latitude);
        c.put(COLUMN_COORDS_LONG, e.longitude);
        c.put(COLUMN_IMPORTANCE, e.importance.ordinal());
        return c;
    }

    public static Note cursorToNote(Cursor c) {
        return c.moveToFirst() ? fromCursor(c) : null;
    }

    private static Note fromCursor(Cursor c) {
        int id = c.getInt(c.getColumnIndexOrThrow(COLUMN_ID));
        String title = c.getString(c.getColumnIndexOrThrow(COLUMN_TITLE));
        String description = c.getString(c.getColumnIndexOrThrow(COLUMN_DESCRIPTION));
        long date = c.getLong(c.getColumnIndexOrThrow(COLUMN_DATE));
        double lat = c.getDouble(c.getColumnIndexOrThrow(COLUMN_COORDS_LAT));
        double lng = c.getDouble(c.getColumnIndexOrThrow(COLUMN_COORDS_LONG));
        Note.Importance i = Note.Importance.values()[c.getInt(c.getColumnIndexOrThrow(COLUMN_IMPORTANCE))];
        return new Note(id, title, description, date, lat, lng, i);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBContract.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
