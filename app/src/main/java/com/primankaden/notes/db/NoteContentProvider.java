package com.primankaden.notes.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.primankaden.notes.App;

import static com.primankaden.notes.db.DBContract.COLUMNS;
import static com.primankaden.notes.db.DBContract.TABLE_NAME;

public class NoteContentProvider extends ContentProvider {
    public static final String AUTHORITY = "com.primankaden.notes.Notes";
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int URI_ALL = 0;

    static {
        URI_MATCHER.addURI(AUTHORITY, TABLE_NAME, URI_ALL);
    }


    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = DataBase.getInstance().getReadableDatabase().query(TABLE_NAME, COLUMNS, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(App.getContext().getContentResolver(), URI);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return "vnd.android.cursor.dir/vnd." + AUTHORITY + "." + TABLE_NAME;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long id = DataBase.getInstance().getReadableDatabase().insert(TABLE_NAME, null, values);
        Uri result = ContentUris.withAppendedId(uri, id);
        App.getContext().getContentResolver().notifyChange(result, null);
        return result;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int result = DataBase.getInstance().getReadableDatabase().delete(TABLE_NAME, selection, selectionArgs);
        App.getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int result = DataBase.getInstance().getReadableDatabase().update(TABLE_NAME, values, selection, selectionArgs);
        App.getContext().getContentResolver().notifyChange(uri, null);
        return result;
    }
}
