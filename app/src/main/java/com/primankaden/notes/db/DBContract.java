package com.primankaden.notes.db;

public final class DBContract {
    public static final String TABLE_NAME = "NOTES";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_COORDS_LAT = "coordsLat";
    public static final String COLUMN_COORDS_LONG = "coordsLong";
    public static final String COLUMN_IMPORTANCE = "importance";

    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
            + COLUMN_ID + " INT INTEGER PRIMARY KEY, "
            + COLUMN_TITLE + " TEXT, "
            + COLUMN_DESCRIPTION + " TEXT, "
            + COLUMN_DATE + " LONG, "
            + COLUMN_COORDS_LAT + " REAL, "
            + COLUMN_COORDS_LONG + " REAL, "
            + COLUMN_IMPORTANCE + " INT)";

    public static final String[] COLUMNS = {
            COLUMN_ID,
            COLUMN_TITLE,
            COLUMN_DESCRIPTION,
            COLUMN_DATE,
            COLUMN_COORDS_LAT,
            COLUMN_COORDS_LONG,
            COLUMN_IMPORTANCE
    };
}
