package com.primankaden.notes.ui.note;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import com.primankaden.notes.db.DataBase;

class NoteLoader extends CursorLoader {
    private int mId;

    public NoteLoader(Context context, int id) {
        super(context);
        mId = id;
    }

    @Override
    public Cursor loadInBackground() {
        return DataBase.getInstance().getNote(mId);
    }
}
