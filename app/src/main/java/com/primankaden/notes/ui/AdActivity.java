package com.primankaden.notes.ui;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.primankaden.notes.App;
import com.primankaden.notes.BuildConfig;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class AdActivity extends AppCompatActivity {
    private final static int BANNER_CHANCE = BuildConfig.DEBUG ? 50 : 10;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestNewInterstitial();
    }

    protected void showRandomBanner() {
        showRandomBanner(BANNER_CHANCE);
    }

    protected void showRandomBanner(int chanceInt) {
        Random rand = new Random();
        if (rand.nextInt(100) < chanceInt) {
            showBannerAd();
        }
    }

    protected void showBannerAd() {
        if (App.getAd().isLoaded()) {
            App.getAd().show();
        }
        requestNewInterstitial();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest;
        if (BuildConfig.DEBUG) {
            adRequest = new AdRequest.Builder().addTestDevice(getDeviceId()).build();
        } else {
            adRequest = new AdRequest.Builder().build();
        }

        App.getAd().loadAd(adRequest);
    }

    private String getDeviceId() {
        if (!BuildConfig.DEBUG) {
            return "";
        }
        String aid = Settings.Secure.getString(App.getContext().getContentResolver(), "android_id");

        Object obj;
        try {
            ((MessageDigest) (obj = MessageDigest.getInstance("MD5"))).update(
                    aid.getBytes(), 0, aid.length());

            obj = String.format("%032X", new BigInteger(1,
                    ((MessageDigest) obj).digest()));
        } catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {
            obj = aid.substring(0, 32);
        }
        return (String) obj;
    }
}