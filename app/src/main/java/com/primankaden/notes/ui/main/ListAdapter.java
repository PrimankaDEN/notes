package com.primankaden.notes.ui.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.primankaden.notes.Note;
import com.primankaden.notes.R;
import com.primankaden.notes.Service;
import com.primankaden.notes.Utils;
import com.primankaden.notes.ui.note.NoteActivity;

import java.util.ArrayList;
import java.util.List;

class ListAdapter extends BaseAdapter {
    private List<Note> mNotes = new ArrayList<>();
    private Context mContext;

    public ListAdapter(Context c) {
        mContext = c;
    }

    public void setData(List<Note> list) {
        mNotes = list;
    }

    @Override
    public int getCount() {
        return mNotes.size();
    }

    @Override
    public Note getItem(int position) {
        return mNotes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mNotes.get(position).id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.v_main_list_row, null);
        }
        Note item = getItem(position);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView description = (TextView) convertView.findViewById(R.id.description);
        TextView date = (TextView) convertView.findViewById(R.id.date);
        if (item.title == null || item.title.isEmpty()) {
            title.setVisibility(View.GONE);
        } else {
            title.setVisibility(View.VISIBLE);
            title.setText(item.title);
        }
        if (item.description == null || item.description.isEmpty()) {
            description.setVisibility(View.GONE);
        } else {
            description.setVisibility(View.VISIBLE);
            description.setText(getItem(position).description);
        }
        date.setText(Utils.formatDate(getItem(position).date));
        convertView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelete(position);
            }
        });
        convertView.findViewById(R.id.edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEdit(position);
            }
        });
        CardView card = (CardView) convertView.findViewById(R.id.card);
        card.setCardBackgroundColor(mContext.getResources().getColor(item.importance.getColorRes()));
        return convertView;
    }

    private void onDelete(final int position) {
        new MaterialDialog.Builder(mContext)
                .title(R.string.discard_question)
                .positiveText(R.string.discard)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Service.deleteNote(mContext, (int) getItemId(position));
                    }
                })
                .show();
    }

    private void onEdit(int position) {
        NoteActivity.startMeForEdit(mContext, getItem(position).id);
    }
}
