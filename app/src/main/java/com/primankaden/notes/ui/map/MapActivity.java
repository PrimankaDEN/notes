package com.primankaden.notes.ui.map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.primankaden.notes.R;

public class MapActivity extends AppCompatActivity {
    public static final String LATITUDE_EXTRA = "latitude",
            LONGITUDE_EXTRA = "longitude";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_map);

    }

    public static void startMeForResult(Activity from, int requestCode, LatLng latLng) {
        Intent i = new Intent(from, MapActivity.class);
        if (latLng != null) {
            i.putExtra(LATITUDE_EXTRA, latLng.latitude);
            i.putExtra(LONGITUDE_EXTRA, latLng.longitude);
        }
        from.startActivityForResult(i, requestCode);
    }
}
