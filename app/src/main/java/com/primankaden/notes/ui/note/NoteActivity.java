package com.primankaden.notes.ui.note;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;
import com.primankaden.notes.Note;
import com.primankaden.notes.R;
import com.primankaden.notes.Service;
import com.primankaden.notes.Utils;
import com.primankaden.notes.db.DataBase;
import com.primankaden.notes.navigation.NavigationHolder;
import com.primankaden.notes.ui.map.MapActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

public class NoteActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, NavigationHolder.LocationListener {
    private static final int EMPTY = -1;
    private int mId;
    private Note mNote;
    protected EditText titleEditText;
    protected EditText descriptionEditText;
    protected TextView date;
    protected Button saveButton, importanceButton;
    protected View content;
    private LatLng mNewLatLng;
    private Note.Importance mNewImportance;
    private final static int NOTE_LOADER = 89794;
    private boolean mNeedUpdateViews = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_note);
        titleEditText = (EditText) findViewById(R.id.title);
        descriptionEditText = (EditText) findViewById(R.id.description);
        date = (TextView) findViewById(R.id.date);
        saveButton = (Button) findViewById(R.id.save);
        content = findViewById(R.id.content);
        importanceButton = (Button) findViewById(R.id.importance);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSave();
            }
        });
        importanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSetImportance();
            }
        });
        mId = getIntent().getIntExtra(NOTE_ID_EXTRA, EMPTY);
        mNeedUpdateViews = savedInstanceState == null;
        if (mId != EMPTY) {
            getSupportLoaderManager().initLoader(NOTE_LOADER, new Bundle(), this);
        } else {
            date.setText(Utils.formatDate(new Date().getTime()));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.m_note, menu);
        if (mId == EMPTY) {
            menu.findItem(R.id.action_download).setVisible(false);
            menu.findItem(R.id.action_delete).setVisible(false);
            menu.findItem(R.id.action_share).setVisible(false);
        }
        return true;
    }

    public void onSetImportance() {
        new MaterialDialog.Builder(this)
                .title(R.string.importance)
                .items(Note.Importance.getTitlesArray())
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        mNewImportance = Note.Importance.values()[which];
                        content.setBackgroundColor(getResources().getColor(mNewImportance.getColorRes()));
                    }
                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        NavigationHolder.getInstance().registerListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        NavigationHolder.getInstance().unregisterListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_download:
                onDownload();
                return true;
            case R.id.action_share:
                mNote.shareMe(this);
                return true;
            case R.id.action_delete:
                new MaterialDialog.Builder(this)
                        .title(R.string.discard_question)
                        .positiveText(R.string.discard)
                        .negativeText(R.string.cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if (mId != EMPTY) {
                                    Service.deleteNote(NoteActivity.this, mId);
                                }
                                finish();
                            }
                        })
                        .show();
                return true;
            case R.id.action_location:
                MapActivity.startMeForResult(this, REQUEST_CODE,
                        mNewLatLng == null ? mNote == null ? null : mNote.getLatLng() : mNewLatLng);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onDownload() {
        File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File(sdCard.getAbsolutePath() + "/Notes");
        if (!directory.mkdirs()) {
            showErrorWriteFile();
            return;
        }

        try {
            File file = new File(directory, mNote.title + mNote.id + ".txt");
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            String str = mNote.toString();
            osw.write(str);
            osw.flush();
            osw.close();
        } catch (IOException e) {
            e.printStackTrace();
            showErrorWriteFile();
            return;
        }
        showSuccessWriteFile();
    }

    private void showSuccessWriteFile() {
        new MaterialDialog.Builder(this)
                .title(R.string.saved)
                .positiveText(R.string.ok)
                .show();
    }

    private void showErrorWriteFile() {
        new MaterialDialog.Builder(this)
                .title(R.string.error_file)
                .positiveText(R.string.ok)
                .show();
    }

    public void onSave() {
        String title = titleEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        if (title.isEmpty() && description.isEmpty()) {
            if (mNote != null) {
                Service.deleteNote(this, mId);
                this.finish();
            }
            return;
        }
        Note note;
        double latitude;
        double longitude;
        if (mNewLatLng == null) {
            if (mNote == null) {
                LatLng ll = NavigationHolder.getInstance().getCurrentLatLng();
                latitude = ll.latitude;
                longitude = ll.longitude;
            } else {
                latitude = mNote.latitude;
                longitude = mNote.longitude;
            }
        } else {
            latitude = mNewLatLng.latitude;
            longitude = mNewLatLng.longitude;
        }
        if (mNote == null) {

            long date = new Date().getTime();
            int id = (int) (date % Integer.MAX_VALUE);
            note = new Note(id, title, description, date, latitude, longitude,
                    mNewImportance == null ? Note.Importance.NONE : mNewImportance);
            Service.addNote(this, note);
        } else {
            note = new Note(mNote.id, title, description, mNote.date, latitude, longitude,
                    mNewImportance == null ? mNote.importance : mNewImportance);
            Service.updateNote(this, note);
        }
        this.finish();
    }

    private static final String NOTE_ID_EXTRA = "noteId";

    public static void startMeForEdit(Context from, int noteId) {
        Intent i = new Intent(from, NoteActivity.class);
        i.putExtra(NOTE_ID_EXTRA, noteId);
        from.startActivity(i);
    }

    public static void startMeForCreate(Context from) {
        Intent i = new Intent(from, NoteActivity.class);
        from.startActivity(i);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new NoteLoader(this, mId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mNote = DataBase.cursorToNote(data);
        date.setText(Utils.formatDate(mNote.date));
        if (mNeedUpdateViews) {
            titleEditText.setText(mNote.title);
            descriptionEditText.setText(mNote.description);
        }
        if (mNewImportance == null) {
            content.setBackgroundColor(getResources().getColor(mNote.importance.getColorRes()));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    private static final int REQUEST_CODE = 456;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra(MapActivity.LATITUDE_EXTRA) && data.hasExtra(MapActivity.LONGITUDE_EXTRA)) {
                    double lat = data.getDoubleExtra(MapActivity.LATITUDE_EXTRA, -1);
                    double lng = data.getDoubleExtra(MapActivity.LONGITUDE_EXTRA, -1);
                    mNewLatLng = new LatLng(lat, lng);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private static final String TITLE_STATE_TAG = "titleState",
            DESCRIPTION_STATE_TAG = "descriptionState",
            IMPORTANCE_STATE_TAG = "importanceState",
            LATITUDE_STATE_TAG = "latState",
            LONGITUDE_STATE_TAG = "longState";

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(TITLE_STATE_TAG, titleEditText.getText().toString());
        outState.putString(DESCRIPTION_STATE_TAG, descriptionEditText.getText().toString());
        if (mNewImportance != null) {
            outState.putInt(IMPORTANCE_STATE_TAG, mNewImportance.ordinal());
        }
        if (mNewLatLng != null) {
            outState.putDouble(LATITUDE_STATE_TAG, mNewLatLng.latitude);
            outState.putDouble(LONGITUDE_STATE_TAG, mNewLatLng.longitude);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle in) {
        if (in.containsKey(TITLE_STATE_TAG)) {
            titleEditText.setText(in.getString(TITLE_STATE_TAG));
        }
        if (in.containsKey(DESCRIPTION_STATE_TAG)) {
            descriptionEditText.setText(in.getString(DESCRIPTION_STATE_TAG));
        }
        if (in.containsKey(LATITUDE_STATE_TAG) && in.containsKey(LONGITUDE_STATE_TAG)) {
            mNewLatLng = new LatLng(
                    in.getDouble(LATITUDE_STATE_TAG),
                    in.getDouble(LONGITUDE_STATE_TAG)
            );
        }
        if (in.containsKey(IMPORTANCE_STATE_TAG)) {
            mNewImportance = Note.Importance.values()[in.getInt(IMPORTANCE_STATE_TAG)];
            content.setBackgroundColor(getResources().getColor(mNewImportance.getColorRes()));
        }
        super.onRestoreInstanceState(in);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mId == EMPTY) {
            mNewLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        }
    }
}
