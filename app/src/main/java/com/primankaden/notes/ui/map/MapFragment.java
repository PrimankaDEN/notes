package com.primankaden.notes.ui.map;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.primankaden.notes.R;
import com.primankaden.notes.Utils;
import com.primankaden.notes.navigation.NavigationHolder;

public class MapFragment extends SupportMapFragment implements OnMapReadyCallback, NavigationHolder.LocationListener {
    private Marker mUserMarker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMapAsync(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        NavigationHolder.getInstance().unregisterListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        NavigationHolder.getInstance().registerListener(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Activity a = getActivity();
                Intent i = new Intent();
                i.putExtra(MapActivity.LATITUDE_EXTRA, latLng.latitude);
                i.putExtra(MapActivity.LONGITUDE_EXTRA, latLng.longitude);
                a.setResult(Activity.RESULT_OK, i);
                a.finish();
            }
        });
        LatLng position = NavigationHolder.getInstance().getCurrentLatLng();
        mUserMarker = map.addMarker(new MarkerOptions().icon(Utils.getBitmapDescriptor(R.layout.v_user_marker)).position(
                position));
        if (getActivity().getIntent().hasExtra(MapActivity.LATITUDE_EXTRA)
                && getActivity().getIntent().hasExtra(MapActivity.LATITUDE_EXTRA)) {
            LatLng ll = new LatLng(getActivity().getIntent().getDoubleExtra(MapActivity.LATITUDE_EXTRA, 0),
                    getActivity().getIntent().getDoubleExtra(MapActivity.LONGITUDE_EXTRA, 0));
            map.addMarker(new MarkerOptions().icon(Utils.getBitmapDescriptor(R.layout.v_note_marker))
                    .anchor(1, 1)
                    .position(ll));
        }

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, ZOOM));
    }

    private final static float ZOOM = 14;

    @Override
    public void onLocationChanged(Location location) {
        if (mUserMarker == null) {
            mUserMarker = getMap().addMarker(new MarkerOptions().icon(Utils.getBitmapDescriptor(R.layout.v_user_marker)).position(
                    NavigationHolder.getInstance().getCurrentLatLng()));
        } else {
            mUserMarker.setPosition(NavigationHolder.getInstance().getCurrentLatLng());
        }
    }
}
