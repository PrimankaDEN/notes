package com.primankaden.notes.ui.main;

import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.primankaden.notes.Note;
import com.primankaden.notes.R;
import com.primankaden.notes.Utils;
import com.primankaden.notes.db.DataBase;
import com.primankaden.notes.navigation.NavigationHolder;
import com.primankaden.notes.ui.note.NoteActivity;

import java.util.List;

public class AllNotesMapFragment extends SupportMapFragment implements OnMapReadyCallback,
        NavigationHolder.LocationListener, LoaderManager.LoaderCallbacks<Cursor>, GoogleMap.OnInfoWindowClickListener {
    private Marker mUserMarker;
    private final static int LOADER_ID = 4653;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMapAsync(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        NavigationHolder.getInstance().registerListener(this);
        getActivity().getSupportLoaderManager().restartLoader(LOADER_ID, new Bundle(), this).forceLoad();
    }

    @Override
    public void onPause() {
        super.onPause();
        NavigationHolder.getInstance().unregisterListener(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.setOnInfoWindowClickListener(this);
    }

    private final static float ZOOM = 14;

    @Override
    public void onLocationChanged(Location location) {
        LatLng ll = new LatLng(location.getLatitude(), location.getLongitude());
        updateUserMarker(ll);
    }

    private void updateUserMarker(LatLng ll) {
        if (mUserMarker == null) {
            getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(ll, ZOOM));
            mUserMarker = getMap().addMarker(new MarkerOptions().icon(Utils.getBitmapDescriptor(R.layout.v_user_marker)).position(ll));
        } else {
            mUserMarker.setPosition(ll);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ListLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        final List<Note> data = DataBase.cursorToNotes(cursor);
        if (data != null && !data.isEmpty())
            getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    map.clear();
                    for (Note n : data) {
                        MarkerOptions marker = new MarkerOptions()
                                .icon(Utils.getBitmapDescriptor(R.layout.v_note_marker))
                                .anchor(1, 1)
                                .position(n.getLatLng())
                                .title(n.title + " " + n.description)
                                .snippet(String.valueOf(n.id));
                        Marker m = map.addMarker(marker);
                    }
                    updateUserMarker(NavigationHolder.getInstance().getCurrentLatLng());
                }
            });
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        int id = Integer.valueOf(marker.getSnippet());
        NoteActivity.startMeForEdit(getActivity(), id);
    }


}
