package com.primankaden.notes.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.primankaden.notes.R;

public class AllNotesMapActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_all_notes_map);
    }

    public static void startMe(Context from) {
        Intent i = new Intent(from, AllNotesMapActivity.class);
        from.startActivity(i);
    }
}
