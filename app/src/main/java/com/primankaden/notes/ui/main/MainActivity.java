package com.primankaden.notes.ui.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.primankaden.notes.Note;
import com.primankaden.notes.R;
import com.primankaden.notes.db.DataBase;
import com.primankaden.notes.ui.AdActivity;
import com.primankaden.notes.ui.note.NoteActivity;

import java.util.List;

public class MainActivity extends AdActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int MY_PERMISSIONS_REQUEST = 5;
    protected ListView listView;
    protected View placeholder;
    private ListAdapter mAdapter;
    private static final int LIST_LOADER = 456;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);
        listView = (ListView) findViewById(R.id.note_list);
        placeholder = findViewById(R.id.placeholder);
        mAdapter = new ListAdapter(this);
        listView.setAdapter(mAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        checkPermissions();
        getSupportLoaderManager().initLoader(LIST_LOADER, new Bundle(), this).forceLoad();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.m_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create:
                NoteActivity.startMeForCreate(MainActivity.this);
                return true;
            case R.id.action_map:
                AllNotesMapActivity.startMe(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ListLoader(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showRandomBanner();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        List<Note> data = DataBase.cursorToNotes(cursor);
        if (data.isEmpty()) {
            listView.setVisibility(View.GONE);
            placeholder.setVisibility(View.VISIBLE);
        } else {
            mAdapter.setData(data);
            listView.setVisibility(View.VISIBLE);
            placeholder.setVisibility(View.GONE);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST);
            }
        }

        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);
            }
        }
    }
}
