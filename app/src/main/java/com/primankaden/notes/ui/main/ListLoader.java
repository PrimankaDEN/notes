package com.primankaden.notes.ui.main;

import android.content.Context;
import android.support.v4.content.CursorLoader;

import com.primankaden.notes.db.DBContract;
import com.primankaden.notes.db.NoteContentProvider;

class ListLoader extends CursorLoader {
    public ListLoader(Context context) {
        super(context, NoteContentProvider.URI, DBContract.COLUMNS, null, null, null);
    }
}
