package com.primankaden.notes.navigation;

import android.location.LocationListener;

abstract class Navigation {
    interface Subscriber extends LocationListener, com.google.android.gms.location.LocationListener {
    }

    protected static final int TIME_TRESHOLD = 1000 * 10;
    protected static final int MOVE_TRESHOLD = 10;

    protected abstract void subscribe(Subscriber subscriber);

    protected abstract void unSubscribe(Subscriber subscriber);

    protected abstract void initialize();
}
