package com.primankaden.notes.navigation;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.primankaden.notes.App;


class NativeNavigation extends Navigation {
    private LocationManager manager;

    NativeNavigation() {
        manager = (LocationManager) App.getContext().getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        if (ActivityCompat.checkSelfPermission(App.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(App.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.requestLocationUpdates(getSafeProvider(), 0, 0, subscriber);
    }

    @Override
    public void unSubscribe(Subscriber subscriber) {
        if (ActivityCompat.checkSelfPermission(App.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(App.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.removeUpdates(subscriber);
    }

    @Override
    public void initialize() {

    }

    protected String getSafeProvider() {
        String provider = manager.getBestProvider(new Criteria(), true);
        if (provider == null || provider.isEmpty()) {
            provider = LocationManager.GPS_PROVIDER;
        }
        return provider;
    }
}
