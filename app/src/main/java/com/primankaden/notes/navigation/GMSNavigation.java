package com.primankaden.notes.navigation;


import android.os.Bundle;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.primankaden.notes.App;


class GMSNavigation extends Navigation {
    private LocationRequest mLocationRequest;
    private GoogleApiClient client;

    @Override
    public void subscribe(final Subscriber subscriber) {
        if (client.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(client, mLocationRequest, subscriber);
        } else {
            client.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(Bundle bundle) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(client, mLocationRequest, subscriber);
                }

                @Override
                public void onConnectionSuspended(int i) {

                }
            });
            client.connect();
        }
    }

    @Override
    public void unSubscribe(Subscriber subscriber) {
        if (client.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(client, mLocationRequest, subscriber);
        }
    }

    @Override
    public void initialize() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(TIME_TRESHOLD);
        mLocationRequest.setFastestInterval(TIME_TRESHOLD);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(MOVE_TRESHOLD);

        client = new GoogleApiClient.Builder(App.getContext())
                .addApi(LocationServices.API)
                .build();
    }
}
