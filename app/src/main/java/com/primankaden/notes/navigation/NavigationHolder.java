package com.primankaden.notes.navigation;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;


import com.google.android.gms.maps.model.LatLng;
import com.primankaden.notes.App;

import java.util.ArrayList;
import java.util.List;

public class NavigationHolder {
    private final static String TAG = "GeoBusinessLogic";
    private static NavigationHolder instance;
    private Navigation mNavigation;
    private List<LocationListener> listeners;
    private Navigation.Subscriber locationListener = new Navigation.Subscriber() {
        @Override
        public void onLocationChanged(Location location) {
            for (LocationListener listener : listeners) {
                listener.onLocationChanged(location);
            }
            lastKnownLocation = location;
            Log.d(TAG, "Location updated, " + listeners.size() + " listeners are notified");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    public static NavigationHolder getInstance() {
        if (instance == null) {
            instance = new NavigationHolder();
        }
        return instance;
    }

    private NavigationHolder() {
        if (App.checkGoogleApiAvailability()) {
            mNavigation = new GMSNavigation();
        } else {
            mNavigation = new NativeNavigation();
        }
        mNavigation.initialize();
        listeners = new ArrayList<>();
    }

    @Nullable
    public Location getCurrentLocation() {
        return lastKnownLocation;
    }

    private Location lastKnownLocation;
    public static final double SAMARA_LATITUDE = 53.2031359;
    public static final double SAMARA_LONGITUDE = 50.1593843;

    @NonNull
    public LatLng getCurrentLatLng() {
        if (lastKnownLocation == null) {
            return new LatLng(SAMARA_LATITUDE, SAMARA_LONGITUDE);
        }
        return new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
    }

    public interface LocationListener {
        void onLocationChanged(Location location);
    }

    public void registerListener(LocationListener listener) {
        if (listeners.isEmpty()) {
            if (ActivityCompat.checkSelfPermission(App.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mNavigation.subscribe(locationListener);
        }
        listeners.add(listener);
    }

    public void unregisterListener(LocationListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }
        if (listeners.isEmpty()) {
            if (ActivityCompat.checkSelfPermission(App.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mNavigation.unSubscribe(locationListener);
        }
    }
}
